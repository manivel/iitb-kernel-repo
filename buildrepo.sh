#!/bin/sh
KEYID=0x00000000
REPODIRECTORY=./repo
PACKAGES=./packages
mkdir -p $REPODIRECTORY/dists/stable/main/binary-amd64
mkdir -p $REPODIRECTORY/pool/main/l/linux/
cp packages/*deb $REPODIRECTORY/pool/main/l/linux/
(cd $PACKAGES;dpkg-scanpackages . | gzip -9 > ../$REPODIRECTORY/dists/stable/main/binary-amd64/Packages.gz)
(cd $PACKAGES;dpkg-scanpackages . > ../$REPODIRECTORY/dists/stable/main/binary-amd64/Packages)
apt-ftparchive -c=aptgenerate.conf release $REPODIRECTORY/dists/stable/ > $REPODIRECTORY/dists/stable/Release
(cd repo/dists/stable;gpg -u $KEYID --armor --detach-sign -o Release.gpg  --sign Release)
